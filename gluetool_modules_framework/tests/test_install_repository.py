# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0

import pytest

from mock import MagicMock, call

import gluetool_modules_framework.libs.guest as guest_module
import gluetool_modules_framework.libs.guest_setup
from gluetool_modules_framework.libs.testing_environment import TestingEnvironment
import gluetool_modules_framework.helpers.install_repository
import gluetool_modules_framework.helpers.rules_engine

from . import create_module, patch_shared


def mock_guest(execute_mock):
    guest_mock = MagicMock()
    guest_mock.name = 'guest0'
    guest_mock.execute = execute_mock

    return guest_mock


@pytest.fixture(name='module')
def fixture_module(monkeypatch):
    module = create_module(gluetool_modules_framework.helpers.install_repository.InstallRepository)[1]

    module._config['log-dir-name'] = 'log-dir-example'
    module._config['download-path'] = 'dummy-path'

    def dummy_testing_farm_request():
        environments_requested = [
            TestingEnvironment(artifacts=[
                {
                    'id': 'https://example.com/repo1',
                    'packages': None,
                    'type': 'repository'
                },
                {
                    'id': 'https://example.com/repo2',
                    'packages': None,
                    'type': 'repository'
                },
                {
                    'id': 'https://example.com/repo3.repo',
                    'type': 'repository-file'
                },
                {
                    'id': 'wrongid',
                    'packages': None,
                    'type': 'wongtype'
                }
            ]),
            TestingEnvironment(artifacts=[
                {
                    'id': 'wrongid',
                    'packages': None,
                    'type': 'wongtype'
                }
            ]),
        ]
        return MagicMock(environments_requested=environments_requested)

    patch_shared(monkeypatch, module, {}, callables={
        'testing_farm_request': dummy_testing_farm_request,
        'evaluate_instructions': gluetool_modules_framework.helpers.rules_engine.RulesEngine.evaluate_instructions,
        'setup_guest': None
    })

    return module


def test_sanity_shared(module):
    assert module.glue.has_shared('setup_guest') is True


@pytest.mark.parametrize('environment_index', [0, 1], ids=['multiple-repositories', 'no-repositories'])
def test_guest_setup(module, environment_index, tmpdir):
    module.execute()

    stage = gluetool_modules_framework.libs.guest_setup.GuestSetupStage.ARTIFACT_INSTALLATION

    execute_mock = MagicMock(return_value=MagicMock(stdout='', stderr=''))
    guest = mock_guest(execute_mock)
    guest.environment = module.shared('testing_farm_request').environments_requested[environment_index]

    module.setup_guest(guest, stage=stage, log_dirpath=str(tmpdir))

    if environment_index == 1:
        execute_mock.assert_has_calls([])
        return

    calls = [
        call('command -v dnf'),
        call('curl --output-dir /etc/yum.repos.d -LO https://example.com/repo3.repo'),
        call('mkdir -pv dummy-path'),
        call('cd dummy-path && dnf repoquery -q --queryformat "%{name}" --repofrompath artifacts-repo,https://example.com/repo1 --disablerepo="*" --enablerepo="artifacts-repo" --location | xargs -n1 curl -sO'),  # noqa
        call('cd dummy-path && dnf repoquery -q --queryformat "%{name}" --repofrompath artifacts-repo,https://example.com/repo2 --disablerepo="*" --enablerepo="artifacts-repo" --location | xargs -n1 curl -sO'),  # noqa
        call('dnf --allowerasing -y reinstall dummy-path/*[^.src].rpm'),
        call('dnf --allowerasing -y downgrade dummy-path/*[^.src].rpm'),
        call('dnf --allowerasing -y update dummy-path/*[^.src].rpm'),
        call('dnf --allowerasing -y install dummy-path/*[^.src].rpm'),
        call("basename --suffix=.rpm dummy-path/*[^.src].rpm | xargs rpm -q")
    ]

    execute_mock.assert_has_calls(calls)
